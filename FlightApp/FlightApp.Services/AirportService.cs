﻿using FlightApp.Core.Interfaces;
using FlightApp.Core.Models;

namespace FlightApp.Services
{
    public class AirportService : IAirportService
    {
        private readonly IAirportRepository _airportRepository;

        public AirportService(IAirportRepository airportRepository)
        {
            _airportRepository = airportRepository;
        }
        public IEnumerable<Airport> GetAllAirports()
        {
            return _airportRepository.GetAllAirports();
        }
    }
}
