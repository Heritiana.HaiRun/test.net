﻿using FlightApp.Core.Interfaces;
using FlightApp.Core.Models;

namespace FlightApp.Services
{
    public class FlightService : IFlightService
    {
        private readonly IFlightRepository _flightRepository;

        public FlightService(IFlightRepository flightRepository)
        {
            _flightRepository = flightRepository;
        }
        public void AddFlight(Flight flight)
        {
            _flightRepository.AddFlight(flight);
        }

        public void DeleteFlight(int id)
        {
            _flightRepository.DeleteFlight(id);
        }

        public IEnumerable<Flight> GetAllFlights()
        {
            return _flightRepository.GetAllFlights();
        }

        public Flight GetFlightById(int id)
        {
            return _flightRepository.GetFlightById(id);
        }

        public void UpdateFlight(Flight flight)
        {
            _flightRepository.UpdateFlight(flight);
        }
    }
}
