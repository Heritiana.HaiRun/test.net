﻿using FlightApp.Core.Interfaces;
using FlightApp.Core.Models;
using FlightApp.Services;
using Moq;

namespace FlightApp.Tests.UnitTests
{
    public class AirportServiceTests
    {
        [Fact]
        public void GetAllAirports_ShouldReturnAllAirports()
        {
            var mockAirportRepository = new Mock<IAirportRepository>();
            mockAirportRepository.Setup(repo => repo.GetAllAirports()).Returns(new List<Airport>
        {
            new Airport { Id = 1, Name = "Airport 1" },
            new Airport { Id = 2, Name = "Airport 2" },
        });

            var airportService = new AirportService(mockAirportRepository.Object);

            var airports = airportService.GetAllAirports();

            Assert.True(airports.Count() == 2, "GetAllAirports should return 2 airports");
        }
    }
}
