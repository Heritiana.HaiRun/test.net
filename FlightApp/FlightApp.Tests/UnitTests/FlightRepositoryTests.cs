﻿using FlightApp.Core.Interfaces;
using FlightApp.Core.Models;
using Moq;

namespace FlightApp.Tests.UnitTests
{

    public class FlightRepositoryTests
    {
        private List<Flight> flights;

        public FlightRepositoryTests()
        {
            flights = new List<Flight>
            {
                new Flight { Id = 1, Name = "Flight 1", DepartureAirportId = 1, ArrivalAirportId = 2 },
                new Flight { Id = 2, Name = "Flight 2", DepartureAirportId = 3, ArrivalAirportId = 2 },
            };
        }

        [Fact]
        public void GetAllFlights_ShouldReturnAllFlights()
        {
            IFlightRepository flightRepository = CreateFlightRepository();

            var flights = flightRepository.GetAllFlights();

            Assert.True(flights.Any(), "No flights were returned when some were expected.");
        }

        [Fact]
        public void GetFlightById_ShouldReturnCorrectFlight()
        {
            IFlightRepository flightRepository = CreateFlightRepository();
            int flightId = 1;

            var flight = flightRepository.GetFlightById(flightId);

            Assert.True(flight != null, "The flight object is null.");
            Assert.True(flightId == flight.Id, "The flight ID does not match the expected value.");
        }

        [Fact]
        public void AddFlight_ShouldAddFlight()
        {
            IFlightRepository flightRepository = CreateFlightRepository();
            var newFlight = new Flight { Name = "New Flight" };

            flightRepository.AddFlight(newFlight);

            var flights = flightRepository.GetAllFlights();
            Assert.True(flights.Contains(newFlight), "The flight was not found in the collection.");

        }

        [Fact]
        public void UpdateFlight_ShouldUpdateFlight()
        {
            IFlightRepository flightRepository = CreateFlightRepository();
            var flightToUpdate = new Flight { Id = 1, Name = "Updated Flight" };

            flightRepository.UpdateFlight(flightToUpdate);

            var updatedFlight = flightRepository.GetFlightById(1);
            Assert.True(flightToUpdate.Name == updatedFlight.Name, "The flight names do not match as expected.");

        }

        [Fact]
        public void DeleteFlight_ShouldDeleteFlight()
        {
            IFlightRepository flightRepository = CreateFlightRepository();
            int flightIdToDelete = 1;

            flightRepository.DeleteFlight(flightIdToDelete);

            var deletedFlight = flightRepository.GetFlightById(flightIdToDelete);
            Assert.True(deletedFlight == null, "The deletedFlight should be null but it is not.");

        }


        private IFlightRepository CreateFlightRepository()
        {
            var mockRepository = new Mock<IFlightRepository>();

            mockRepository.Setup(repo => repo.GetAllFlights())
                .Returns(() => flights);

            mockRepository.Setup(repo => repo.GetFlightById(It.IsAny<int>()))
                .Returns((int flightId) =>
                {
                    return flights.FirstOrDefault(f => f.Id == flightId);
                });

            mockRepository.Setup(repo => repo.AddFlight(It.IsAny<Flight>()))
                .Callback((Flight newFlight) =>
                {
                    flights.Add(newFlight);
                });

            mockRepository.Setup(repo => repo.UpdateFlight(It.IsAny<Flight>()))
                .Callback((Flight updatedFlight) =>
                {
                    var existingFlight = flights.FirstOrDefault(f => f.Id == updatedFlight.Id);
                    if (existingFlight != null)
                    {
                        existingFlight.Name = updatedFlight.Name;
                        existingFlight.DepartureAirportId = updatedFlight.DepartureAirportId;
                        existingFlight.ArrivalAirportId = updatedFlight.ArrivalAirportId;
                    }
                });

            mockRepository.Setup(repo => repo.DeleteFlight(It.IsAny<int>()))
                .Callback((int flightId) =>
                {
                    var flightToDelete = flights.FirstOrDefault(f => f.Id == flightId);
                    if (flightToDelete != null)
                    {
                        flights.Remove(flightToDelete);
                    }
                });

            return mockRepository.Object;
        }

    }

}
