﻿using FlightApp.Core.Models;

namespace FlightApp.Tests.UnitTests
{
    public class FlightDistanceTests
    {
        [Fact]
        public void CalculateDistance_ShouldCalculateDistanceCorrectly()
        {
            var departureAirport = new Airport { Latitude = 25.3286, Longitude = 55.5172 };
            var arrivalAirport = new Airport { Latitude = 24.4283, Longitude = 54.4581 };
            var flight = new Flight
            {
                DepartureAirport = departureAirport,
                ArrivalAirport = arrivalAirport
            };

            double distance = flight.CalculateDistance();

            Assert.True(distance != 0, "");
        }
    }
}
