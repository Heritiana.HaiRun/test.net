﻿using FlightApp.Core.Interfaces;
using FlightApp.Core.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightApp.Tests.UnitTests
{
    public class AirportRepositoryTests
    {
        [Fact]
        public void GetAllAirports_ShouldReturnAllAirports()
        {
            IAirportRepository airportRepository = CreateAirportRepository();

            var airports = airportRepository.GetAllAirports();

            Assert.True(airports.Any(), "The 'airports' collection should not be empty.");
        }

        private IAirportRepository CreateAirportRepository()
        {
            var mockRepository = new Mock<IAirportRepository>();

            mockRepository.Setup(repo => repo.GetAllAirports())
                .Returns(new List<Airport>
                {
                    new Airport { Id = 1, Name = "Airport 1" },
                    new Airport { Id = 2, Name = "Airport 2" },
                });

            return mockRepository.Object;
        }
    }
}
