using FlightApp.Core.Interfaces;
using FlightApp.Core.Models;
using FlightApp.Services;
using Moq;

namespace FlightApp.Tests.UnitTests
{
    public class FlightServiceTests
    {
        [Fact]
        public void AddFlight_ShouldAddFlight()
        {
            var mockFlightRepository = new Mock<IFlightRepository>();
            var flightService = new FlightService(mockFlightRepository.Object);
            var flight = new Flight { Id = 1, Name = "Test Flight", DepartureAirportId = 1, ArrivalAirportId = 2 };

            flightService.AddFlight(flight);

            mockFlightRepository.Verify(repo => repo.AddFlight(flight), Times.Once, "The AddFlight method should have been called once with the provided flight.");

        }

        [Fact]
        public void DeleteFlight_ShouldDeleteFlight()
        {
            var mockFlightRepository = new Mock<IFlightRepository>();
            var flightService = new FlightService(mockFlightRepository.Object);
            var flightId = 1;

            flightService.DeleteFlight(flightId);

            mockFlightRepository.Verify(repo => repo.DeleteFlight(flightId), Times.Once, "Expected DeleteFlight to be called once with the provided flightId.");
        }


        [Fact]
        public void GetAllFlights_ShouldReturnAllFlights()
        {
            var mockFlightRepository = new Mock<IFlightRepository>();
            mockFlightRepository.Setup(repo => repo.GetAllFlights()).Returns(new List<Flight>
        {
            new Flight { Id = 1, Name = "Flight 1", DepartureAirportId = 1, ArrivalAirportId = 2 },
            new Flight { Id = 2, Name = "Flight 2", DepartureAirportId = 3, ArrivalAirportId = 2 },
        });

            var flightService = new FlightService(mockFlightRepository.Object);

            var flights = flightService.GetAllFlights();

            Assert.True(flights.Count() == 2, "GetAllFlights should return 2 flights");
        }

        [Fact]
        public void GetFlightById_ShouldReturnFlight()
        {
            var mockFlightRepository = new Mock<IFlightRepository>();
            var expectedFlight = new Flight { Id = 1, Name = "Test Flight", ArrivalAirportId = 1, DepartureAirportId = 2 };
            mockFlightRepository.Setup(repo => repo.GetFlightById(expectedFlight.Id)).Returns(expectedFlight);

            var flightService = new FlightService(mockFlightRepository.Object);

            var flight = flightService.GetFlightById(expectedFlight.Id);

            Assert.True(flight == expectedFlight, "GetFlightById should return expected flight");
        }
    }

}