﻿using FlightApp.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightApp.Core.Interfaces
{
    public interface IAirportRepository
    {
        IEnumerable<Airport> GetAllAirports();
    }
}
