﻿using FlightApp.Core.Models;

namespace FlightApp.Core.Interfaces
{
    public interface IFlightService
    {
        IEnumerable<Flight> GetAllFlights();
        Flight GetFlightById(int id);
        void AddFlight(Flight flight);
        void UpdateFlight(Flight flight);
        void DeleteFlight(int id);
    }
}
