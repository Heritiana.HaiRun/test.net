﻿using FlightApp.Core.Models;

namespace FlightApp.Core.Interfaces
{
    public interface IAirportService
    {
        IEnumerable<Airport> GetAllAirports();
    }
}
