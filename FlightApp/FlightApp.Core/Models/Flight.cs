﻿namespace FlightApp.Core.Models
{
    public class Flight
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int DepartureAirportId { get; set; }
        public int ArrivalAirportId { get; set; }
        public Airport DepartureAirport { get; set; }
        public Airport ArrivalAirport { get; set; }

        public double CalculateDistance()
        {
            double lat1 = DepartureAirport.Latitude;
            double lon1 = DepartureAirport.Longitude;
            double lat2 = ArrivalAirport.Latitude;
            double lon2 = ArrivalAirport.Longitude;

            double dLat = (lat2 - lat1) * (Math.PI / 180);
            double dLon = (lon2 - lon1) * (Math.PI / 180);

            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                       Math.Cos(lat1 * (Math.PI / 180)) * Math.Cos(lat2 * (Math.PI / 180)) *
                       Math.Sin(dLon / 2) * Math.Sin(dLon / 2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            double distance = 6371 * c;

            return distance;
        }

    }
}
