﻿using FlightApp.Core.Interfaces;
using FlightApp.Core.Models;

namespace FlightApp.Data.Repositories
{
    public class AirportRepository : IAirportRepository
    {
        private readonly DBContext _context;
        public AirportRepository(DBContext context)
        {
            _context = context;
        }
        public IEnumerable<Airport> GetAllAirports()
        {
            return _context.Airports.ToList();
        }
    }
}
