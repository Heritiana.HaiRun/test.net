﻿using FlightApp.Core.Interfaces;
using FlightApp.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace FlightApp.Data.Repositories
{
    public class FlightRepository : IFlightRepository
    {
        private readonly DBContext _context;

        public FlightRepository(DBContext context)
        {
            _context = context;
        }
        public void AddFlight(Flight flight)
        {
            _context.Flights.Add(flight);
            _context.SaveChanges();
        }

        public void DeleteFlight(int id)
        {
            var flight = _context.Flights.Find(id);
            if (flight != null)
            {
                _context.Flights.Remove(flight);
                _context.SaveChanges();
            }
        }

        public IEnumerable<Flight> GetAllFlights()
        {
            return _context.Flights.Include(f => f.ArrivalAirport).Include(f => f.DepartureAirport).ToList();
        }

        public Flight GetFlightById(int id)
        {
            return _context.Flights.FirstOrDefault(f => f.Id == id);
        }

        public void UpdateFlight(Flight flight)
        {
            _context.Entry(flight).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
