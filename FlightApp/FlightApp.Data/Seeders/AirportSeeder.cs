﻿using FlightApp.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace FlightApp.Data.Seeders
{
    public static class AirportSeeder
    {
        public static void SeedAirports(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Airport>().HasData(
                new Airport { Id = 1, Name = "Sharjah International Airport", Latitude = 25.3286, Longitude = 55.5172 },
                new Airport { Id = 2, Name = "Al Bateen Executive Airport", Latitude = 24.4283, Longitude = 54.4581 },
                new Airport { Id = 3, Name = "Fujairah International Airport", Latitude = 25.1122, Longitude = 56.324 },
                new Airport { Id = 4, Name = "Sir Bani Yas Airport", Latitude = 24.2836, Longitude = 52.5803 },
                new Airport { Id = 5, Name = "Ras Al Khaimah International Airport", Latitude = 25.6135, Longitude = 55.9388 },
                new Airport { Id = 6, Name = "Al Maktoum International Airport", Latitude = 24.8964, Longitude = 55.1614 }
            );
        }
    }
}
