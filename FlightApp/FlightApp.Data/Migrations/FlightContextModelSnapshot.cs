﻿// <auto-generated />
using FlightApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace FlightApp.Data.Migrations
{
    [DbContext(typeof(DBContext))]
    partial class FlightContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.12")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("FlightApp.Core.Models.Airport", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<double>("Latitude")
                        .HasColumnType("float");

                    b.Property<double>("Longitude")
                        .HasColumnType("float");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Airports");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Latitude = 25.328600000000002,
                            Longitude = 55.517200000000003,
                            Name = "Sharjah International Airport"
                        },
                        new
                        {
                            Id = 2,
                            Latitude = 24.4283,
                            Longitude = 54.458100000000002,
                            Name = "Al Bateen Executive Airport"
                        },
                        new
                        {
                            Id = 3,
                            Latitude = 25.112200000000001,
                            Longitude = 56.323999999999998,
                            Name = "Fujairah International Airport"
                        },
                        new
                        {
                            Id = 4,
                            Latitude = 24.2836,
                            Longitude = 52.580300000000001,
                            Name = "Sir Bani Yas Airport"
                        },
                        new
                        {
                            Id = 5,
                            Latitude = 25.613499999999998,
                            Longitude = 55.938800000000001,
                            Name = "Ras Al Khaimah International Airport"
                        },
                        new
                        {
                            Id = 6,
                            Latitude = 24.8964,
                            Longitude = 55.1614,
                            Name = "Al Maktoum International Airport"
                        });
                });

            modelBuilder.Entity("FlightApp.Core.Models.Flight", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<int>("ArrivalAirportId")
                        .HasColumnType("int");

                    b.Property<int>("DepartureAirportId")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("ArrivalAirportId");

                    b.HasIndex("DepartureAirportId");

                    b.ToTable("Flights");
                });

            modelBuilder.Entity("FlightApp.Core.Models.Flight", b =>
                {
                    b.HasOne("FlightApp.Core.Models.Airport", "ArrivalAirport")
                        .WithMany()
                        .HasForeignKey("ArrivalAirportId")
                        .OnDelete(DeleteBehavior.NoAction)
                        .IsRequired();

                    b.HasOne("FlightApp.Core.Models.Airport", "DepartureAirport")
                        .WithMany()
                        .HasForeignKey("DepartureAirportId")
                        .OnDelete(DeleteBehavior.NoAction)
                        .IsRequired();

                    b.Navigation("ArrivalAirport");

                    b.Navigation("DepartureAirport");
                });
#pragma warning restore 612, 618
        }
    }
}
