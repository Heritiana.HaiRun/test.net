﻿using FlightApp.Core.Models;
using FlightApp.Data.Seeders;
using Microsoft.EntityFrameworkCore;

namespace FlightApp.Data
{
    public class DBContext : DbContext
    {
        public DBContext(DbContextOptions<DBContext> options) : base(options) { }

        public DbSet<Flight> Flights { get; set; }
        public DbSet<Airport> Airports { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Flight>(entity =>
            {
                entity.HasOne(flight => flight.DepartureAirport)
                    .WithMany()
                    .HasForeignKey(flight => flight.DepartureAirportId)
                    .OnDelete(DeleteBehavior.NoAction);

                entity.HasOne(flight => flight.ArrivalAirport)
                    .WithMany()
                    .HasForeignKey(flight => flight.ArrivalAirportId)
                    .OnDelete(DeleteBehavior.NoAction);
            });

            AirportSeeder.SeedAirports(modelBuilder); // Seed airport data using the AirportSeeder.
            base.OnModelCreating(modelBuilder);
        }
    }
}
