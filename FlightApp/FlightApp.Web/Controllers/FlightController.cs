﻿using FlightApp.Core.Interfaces;
using FlightApp.Core.Models;
using FlightApp.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace FlightApp.Web.Controllers
{
    public class FlightController : Controller
    {
        private readonly IFlightService _flightService;
        private readonly IAirportService _airportService;
        private readonly ILogger<FlightController> _logger;

        public FlightController(IFlightService flightService, IAirportService airportService,ILogger<FlightController> logger)
        {
            _flightService = flightService;
            _airportService = airportService;
            _logger = logger;
        }
        public IActionResult Index()
        {
            try
            {
                var flights = _flightService.GetAllFlights();
                var flightViewModels = flights.Select(flight => new FlightListViewModel
                {
                    Id = flight.Id,
                    Name = flight.Name,
                    DepartureAirportName = flight.DepartureAirport.Name,
                    ArrivalAirportName = flight.ArrivalAirport.Name,
                    Distance = flight.CalculateDistance()
                }).ToList();

                return View(flightViewModels);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while retrieving flight data.");
                return View("Error", new ErrorViewModel { ErrorMessage = "An error occurred while retrieving flight data." }); 
                
            }
        }

        public IActionResult Create()
        {
            var viewModel = new FlightViewModel
            {
                AvailableAirports = (List<Airport>)_airportService.GetAllAirports(),
            };

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(FlightViewModel flightViewModel)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    var flight = new Flight
                    {
                        Name = flightViewModel.Name,
                        DepartureAirportId = flightViewModel.DepartureAirportId,
                        ArrivalAirportId = flightViewModel.ArrivalAirportId,
                    };

                    _flightService.AddFlight(flight);

                    return RedirectToAction("Index");
                }

                flightViewModel.AvailableAirports = (List<Airport>)_airportService.GetAllAirports();

                return View(flightViewModel);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while creating a flight.");
                return View("Error", new ErrorViewModel { ErrorMessage = "An error occurred while creating a flight." });
            }
            
        }

        public IActionResult Edit(int id)
        {
            try
            {

                var flight = _flightService.GetFlightById(id);
                if (flight == null)
                {
                    return NotFound();
                }
                var viewModel = new FlightViewModel
                {
                    Id = flight.Id,
                    Name = flight.Name,
                    DepartureAirportId = flight.DepartureAirportId,
                    ArrivalAirportId = flight.ArrivalAirportId,
                    AvailableAirports = (List<Airport>)_airportService.GetAllAirports(),
                };


                return View(viewModel);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while retrieving flight data for editing.");
                return View("Error", new ErrorViewModel { ErrorMessage = "An error occurred while retrieving flight data for editing." });

            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(FlightViewModel viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        var flight = _flightService.GetFlightById(viewModel.Id);
                        flight.Name = viewModel.Name;
                        flight.ArrivalAirportId = viewModel.ArrivalAirportId;
                        flight.DepartureAirportId = viewModel.DepartureAirportId;
                        _flightService.UpdateFlight(flight);
                    }
                    catch
                    {
                        return NotFound();
                    }
                    return RedirectToAction("Index");
                }

                viewModel.AvailableAirports = (List<Airport>)_airportService.GetAllAirports();
                return View(viewModel);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while updating the flight.");
                return View("Error", new ErrorViewModel { ErrorMessage = "An error occurred while updating the flight." });
            }
        }

        public IActionResult Delete(int id)
        {
            try
            {
                var flight = _flightService.GetFlightById(id);
            
                if (flight == null)
                {
                    return NotFound();
                }
                return View(flight);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while retrieving flight data for deletion.");
                return View("Error", new ErrorViewModel { ErrorMessage = "An error occurred while retrieving flight data for deletion." });
            }
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            try
            {
                _flightService.DeleteFlight(id);
                return RedirectToAction("Index");

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while deleting flight data.");
                return View("Error", new ErrorViewModel { ErrorMessage = "An error occurred while deleting flight data." });
            }
        }
    }
}
