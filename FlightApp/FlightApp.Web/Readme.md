﻿# FlightApp

FlightApp is a tiny web application developed using ASP.NET MVC and bootstrap that allows users to manage and view a list of flights. It provides features for adding, deleting, and modifying flights and displays the computed distance between departure and arrival airports.

## Table of Contents

- [FlightApp](#flightapp)
  - [Table of Contents](#table-of-contents)
  - [Introduction](#introduction)
  - [Features](#features))
  - [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
  - [Database Setup](#database-setup)
    - [Connection String](#connection-string)
    - [Applying Migrations](#applying-migrations)
  - [Usage](#usage)
  - [Project Structure](#project-structure)
  - [Technologies Used](#technologies-used)

## Introduction

FlightApp is a simple web application designed to manage flight data. It provides an easy-to-use interface to add, delete, and modify flights. Each flight consists of a name, a departure airport, and an arrival airport, with the computed distance between them. The application is built using ASP.NET MVC and follows a clean project structure to separate concerns and improve maintainability.

## Features

- User-friendly interface to manage flights.
- Add new flights with names, departure airports, and arrival airports.
- Delete existing flights from the list.
- Modify flight details, including airports and names.
- Automatic computation of the distance between departure and arrival airports using GPS coordinates.

## Getting Started

### Prerequisites

Before you can run FlightApp, you need to have the following prerequisites installed:

- [.NET Core SDK](https://dotnet.microsoft.com/download)
- [Install SQL Server](https://www.microsoft.com/en-us/sql-server/sql-server-downloads) if you haven't already.

## Database Setup

### Connection String

Make sure to update your appsettings.json file in the FlightApp.Web project with the connection string for your SQL Server instance. Modify the following section:

```bash
"ConnectionStrings": {
    "FlightContext": "Server=YourServerName;Database=YourDbName;Trusted_Connection=True;"
}
```

### Applying Migrations

Apply migrations using Entity Framework. Run the following commands in the Package Manager Console:

```bash
Update-Database
```

Then run the project.

FlightApp will start, and you can access it in your web browser at <http://localhost:5101/>.

## Usage

Visit the FlightApp web application in your browser.
Use the user interface to add, delete, and modify flights.
The computed distance between departure and arrival airports is displayed for each flight.

## Project Structure

FlightApp follows a structured project layout:

```bash
FlightApp (Solution)
│
├── FlightApp.Core
│   ├── Models
│   ├── Interfaces
│
├── FlightApp.Data
│   ├── DBContext
│   ├── Migrations
│   ├── Repositories
│
├── FlightApp.Services
│   ├── FlightService.cs
│
├── FlightApp.Tests
│   ├── UnitTests
│
├── FlightApp.Web
│   ├── Controllers
│   ├── Views
|   |── ViewModels
│   ├── wwwroot
│   │   ├── css
│   │   └── js
│   ├── appsettings.json
│   ├── program.cs
```

- FlightApp.Web: Contains the main ASP.NET MVC application logic, including controllers, views, and static files.
- FlightApp.Core: Houses core business logic and domain models, including the Flight model and interfaces for repositories and services.
- FlightApp.Data: Handles data access and database operations, including the FlightContext and FlightRepository.
- FlightApp.Services: Contains service layer classes to handle application logic, such as the FlightService.
- FlightApp.Tests: Includes unit tests for the application.

## Technologies Used

- ASP.NET Core MVC
- Entity Framework Core
- C#
- HTML/CSS
- Bootstap
- Razor Views
- xUnit (for unit testing)
