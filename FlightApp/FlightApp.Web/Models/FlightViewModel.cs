﻿using FlightApp.Core.Models;
using System.ComponentModel.DataAnnotations;

namespace FlightApp.Web.Models
{
    public class FlightViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter the flight name.")]
        [Display(Name = "Flight Name")]
        public string Name { get; set; }
        [Display(Name = "Departure Airport")]
        public int DepartureAirportId { get; set; }
        [Display(Name = "Arrival Airport")]
        public int ArrivalAirportId { get; set; }
        public List<Airport> AvailableAirports { get; set; } = new List<Airport>();
    }
}
