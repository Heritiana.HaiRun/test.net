﻿using FlightApp.Core.Models;
using System.ComponentModel.DataAnnotations;

namespace FlightApp.Web.Models
{

    public class FlightListViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DepartureAirportName { get; set; }
        public string ArrivalAirportName { get; set; }
        public double Distance { get; set; }
    }

}
