﻿using System;

public static class ExpressionEvaluator
{
    public static int Evaluate(string expression)
    {
        string[] tokens = expression.Split(' ');

        // Check if the expression has the correct format. It should have at least 3 tokens,
        // and the number of tokens should be odd (e.g., "3 + 4 * 2" is valid, "3 + 4 +" is not).
        if (tokens.Length < 3 || tokens.Length % 2 == 0)
        {
            throw new ArgumentException("Invalid expression: " + expression);
        }

        int result = int.Parse(tokens[0]);
        for (int i = 1; i < tokens.Length; i += 2)
        {
            // Get the operator and the next operand.
            char op = tokens[i][0];
            int operand = int.Parse(tokens[i + 1]);

            // Check if there is a higher-priority operator following.
            if (i + 2 < tokens.Length)
            {
                char nextOp = tokens[i + 2][0];

                // If the next operator has higher priority, apply it to the current operand.
                if (GetOperatorPriority(nextOp) > GetOperatorPriority(op))
                {
                    int nextOperand = int.Parse(tokens[i + 3]);
                    operand = ApplyOperator(operand, nextOp, nextOperand);
                    i += 2;
                }
            }

            result = ApplyOperator(result, op, operand);
        }

        return result;
    }

    private static int GetOperatorPriority(char op)
    {
        switch (op)
        {
            case '+':
            case '-':
                return 1;
            case '*':
                return 2;
            default:
                return 0;
        }
    }


    private static int ApplyOperator(int leftOperand, char op, int rightOperand)
    {
        return op switch
        {
            '+' => leftOperand + rightOperand,
            '-' => leftOperand - rightOperand,
            '*' => leftOperand * rightOperand,
            _ => throw new ArgumentException("Invalid operator: " + op),
        };
    }

}
