﻿
while (true)
{
    Console.Write("Enter an expression (e.g., 1 + 2): ");
    string expression = Console.ReadLine();
    try
    {
        int result = ExpressionEvaluator.Evaluate(expression);
        Console.WriteLine($"{expression} = {result}");
    }
    catch (Exception ex)
    {
        Console.WriteLine($"Error: {ex.Message}");
    }
    
    Console.Write("Do you want to evaluate another expression? (y/n): ");
    string continueOption = Console.ReadLine().ToLower();

    if (continueOption != "y")
    {
        break;
    }
}