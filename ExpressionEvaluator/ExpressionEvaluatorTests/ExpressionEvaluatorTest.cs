﻿
namespace ExpressionEvaluatorTests
{
    public class ExpressionEvaluatorTest
    {
        [Theory]
        [InlineData("1 + 2", 3)]
        [InlineData("3 * 4", 12)]
        [InlineData("11 - 2", 9)]
        [InlineData("2 * 3 - 1", 5)]
        [InlineData("6 - 2 * 5", -4)]
        public void Evaluate_ValidExpressions_ReturnsExpectedResult(string expression, int expected)
        {
            int result = ExpressionEvaluator.Evaluate(expression);
            Assert.True(expected == result, $"The result of the expression '{expression}' is not equal to the expected value '{expected}'.");
        }
    }
}
